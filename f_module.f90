! A dummy Fortran module to test interoperability with C
module F_Module
  use ISO_C_Binding
  interface
     function Add(lhs, rhs) bind(c, name = "Add")
       use ISO_C_Binding
       integer (c_int) :: Area ! Function return type
       ! Since Fortran is pass-by-reference by default,
       ! and C isn't, we have to pass lhs and rhs by value
       integer (c_int), value :: lhs, rhs
     end function Add
  end interface
end module F_Module
