# Makefile to build and clean this project easily

F_Driver :	f_driver.f90 f_module.o c_function.o
		gfortran -o F_Driver -Wall f_driver.f90 f_module.o c_function.o

f_module.o :	f_module.f90
		gfortran -c f_module.f90

c_function.o :	c_function.c
		gcc -c c_function.c

clean :
	rm -f *.o *.mod F_Driver