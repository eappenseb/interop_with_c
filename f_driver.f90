! A dummy Fortran driver to test interoperability with C
program F_Driver
  use ISO_C_Binding
  use F_Module
  implicit none

  integer(c_int) :: lhs, rhs

  print *, "Enter in values for lhs and rhs to be added together: "
  read *, lhs, rhs

  print *, "Result is: ", Add(lhs, rhs)
end program F_Driver
